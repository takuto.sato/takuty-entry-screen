#!/bin/bash
set -eux

declare -a BRANCH_STAGE_NAME=$1

# 各環境に合わせてAmplifyConfig.jsを差し替える。
function set_baseConfigJS(){
      cp ./_env/algoliaConfigType.ts src/config/algoliaConfigType.ts
      cp ./_env/${BRANCH_STAGE_NAME}_baseConfig.ts src/config/baseConfig.ts
}

## MAIN
set_baseConfigJS
