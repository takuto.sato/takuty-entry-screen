#!/bin/bash
set -eux

BRANCH_STAGE_NAME=$1
AWS_ACCESS_KEY=''
AWS_SECRET_ACCESS_KEY=''
S3_BUCKET_NAME=''
CLOUDFRONT_DISTORIBUTION=''


function setupAwsCredential(){
      unsetDebug
      if [[ ${BRANCH_STAGE_NAME} = "develop" ]];then
            echo "branch is $BRANCH_STAGE_NAME"
            AWS_ACCESS_KEY=${AWS_ACCESS_KEY_ID_develop}
            AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY_develop}
            S3_BUCKET_NAME=${S3_BUCKET_NAME_develop}
            CLOUDFRONT_DISTORIBUTION=${CLOUDFRONT_DISTORIBUTION_develop}
      elif [[ ${BRANCH_STAGE_NAME} = "staging" ]];then
            echo "branch is $BRANCH_STAGE_NAME"
            AWS_ACCESS_KEY=${AWS_ACCESS_KEY_ID_staging}
            AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY_staging}
            S3_BUCKET_NAME=${S3_BUCKET_NAME_staging}
            CLOUDFRONT_DISTORIBUTION=${CLOUDFRONT_DISTORIBUTION_staging}
      elif [[ ${BRANCH_STAGE_NAME} = "production" ]];then
            echo "branch is $BRANCH_STAGE_NAME"
            AWS_ACCESS_KEY=${AWS_ACCESS_KEY_ID_production}
            AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY_production}
            S3_BUCKET_NAME=${S3_BUCKET_NAME_production}
            CLOUDFRONT_DISTORIBUTION=${CLOUDFRONT_DISTORIBUTION_production}
      fi
      setDebug
}

function setAwsConfig(){
      unsetDebug
      aws configure set aws_access_key_id ${AWS_ACCESS_KEY}
      aws configure set aws_secret_access_key ${AWS_SECRET_ACCESS_KEY}
      aws configure set output json
      aws configure set region ap-northeast-1
      setDebug
}

function deploy(){
      aws s3 sync ./build/ s3://${S3_BUCKET_NAME}/ --acl public-read --cache-control "max-age=0"  --profile=default
      aws s3 cp ./src/static/pdf/sumamoruchintai.pdf s3://${S3_BUCKET_NAME}/static/ --acl public-read --cache-control "max-age=3600"  --profile=default
}

function clearCacheforCloudFront(){
    aws configure set preview.cloudfront true 
	aws cloudfront create-invalidation --distribution-id ${CLOUDFRONT_DISTORIBUTION} --paths '/*'
}

function setDebug(){
      set -x
}

function unsetDebug(){
      set +x
}

setupAwsCredential
setAwsConfig
deploy
clearCacheforCloudFront