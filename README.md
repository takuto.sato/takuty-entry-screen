## ブランチ戦略
基本的には以下のブランチのみが存在している状態が正となる。

ブランチ名 | 解説
-- | --
develop | 開発ブランチ。機能追加や緊急度の低いバグ修正が入る
staging | 評価用ブランチ。developからマージされることでのみ育つ (2019/08時点で該当環境はなし)
hotfix | バグ修正ブランチ。緊急度の高いバグ修正が入る (2019/08時点で該当環境はなし)
production | 本番ブランチ。stagingあるいはhotfixからマージされることでのみ育つ

## 命名規則に関して
* コンポーネントを含むファイル(.tsx)はアッパーキャメルケース
* その他ファイルはケバブケース
* 原則変数名はローワーキャメルケース
* selling-common以下に記載のタイプに関してはスネークケース(テーブルカラムに直結する項目が大半であり、テーブルのカラム名がスネークケースであるため)
* 定数はコンスタントケース

## プロジェクトの構成
* _ci : gitlabでビルドデプロイする際に何をするかまとめたやつ。
* _env : 各環境ごとに使い分ける必要がある情報をまとめたやつ(AWSの認証情報やAlgoliaの情報など)
* src/algolia : アルゴリアの設定情報や基本情報
* src/components : コンポーネント情報

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## 開発するとき

環境情報を Develop にする

```sh
cp ./_env/develop_baseConfig.ts ./src/config/baseConfig.ts
```

起動

```sh
npm start
```
## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (Webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
