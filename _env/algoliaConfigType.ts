export type AlgoliaConfigType = {
  AllSpace: AlgoliaConfigTableUnit;
};

export type AlgoliaConfigTableUnit = {
  indexName: string;
  applicationId: string;
  searchOnlyApiKey: string;
  adminApiKey: string;
  monitoringApiKey: string;
};
