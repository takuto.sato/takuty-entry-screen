import { ItemValueKey } from '../../config/item-value-key';
import { ConfirmationDisplayType } from '../../enums/confirmation/confirmation-display-type';
import { ComponentDescriptionType } from '../../enums/common/component-description-type';
import { ConditionsResultJudgeRule } from '../../enums/common/conditions-result-judge-rule';
import { ValueOriginType } from '../../enums/common/value-origin-type';
import { ComponentType } from '../../enums/common/component-type';
import { DetailConditionOperator } from '../../enums/common/detail-condition-operator';

// 申し込み対象・お引越し情報・お客様情報などのセクションごとの情報
export interface ContainerElement {
  id: string;
  title?: string;
  confirmationTitle?: string; // 確認ページで表示するタイトル、未指定(undefined)の場合にはtitleが使用される、確認ページのみタイトル未表示としたい場合には空文字を指定する
  components?: Array<ComponentElement>;
  confirmationDisplayType?: ConfirmationDisplayType;
}

export interface ComponentElement {
  id: string;
  label?: string;
  confirmationLabel?: string;
  isDisplayCondition?: Array<ComponentElementCondition>;
  descriptionsBeforeInput?: Array<ComponentDescriptionElement>;
  inputElement?: ComponentInputElement;
  inputElements?: Array<ComponentInputElement>;
  descriptionsAfterInput?: Array<ComponentDescriptionElement>;
  showOnlyConfirmation?: boolean;
  showOnlyInput?: boolean;
}

export interface ComponentInputElement {
  valueKey: ItemValueKey | string; // 入力値をマップに保存する際に使用するキー情報
  // valueKeys?: Array<ItemValueKey>; // 入力値をマップに保存する際に使用するキー情報
  inputType: ComponentType;
  initialValue?: string | number | boolean;
  valueType?: ValueOriginType;
  value?: string | number | boolean;
  inputComponentType?: string;
  placeholder?: string;
  converter?: (value: any) => string;
  selectionItems?: Array<InputComponentSelectionItem>;
  isDisableCondition?: Array<ComponentElementCondition>;
  styleOption?: any;
  minDateOffset?: number; // 未来日付を正としてサイト起動時の現在日付を基準とする
  maxDateOffset?: number; // 未来日付を正としてサイト起動時の現在日付を基準とする
}

export interface InputComponentSelectionItem {
  value: string | number;
  label: string;
  notDisplay?: boolean;
  disabled?: boolean;
}

export interface ComponentDescriptionElement {
  descriptionType?: ComponentDescriptionType; // デフォルトはNormal
  messages: Array<string>;
}

// 条件
export interface ComponentElementCondition {
  // conditionType?: ComponentElementConditionType;
  judgeRuleType?: ConditionsResultJudgeRule; // 詳細条件の判定条件、デフォルトはすべてのdetailConditionがTrueの場合のみTrueとなるEvery
  detailConditions: Array<ComponentElementDetailCondition>;
}

// export enum ComponentElementConditionType {
//   ExitIfTrue = 'ExitIfTrue',
//   ExitIfFalse = 'ExitIfFalse'
// }

export interface ComponentElementDetailCondition {
  valueType?: ValueOriginType;
  valueKey: string;
  operator: DetailConditionOperator;
  targetValues: Array<{
    valueType?: ValueOriginType;
    valueKey: string | boolean | number;
  }>;
}
