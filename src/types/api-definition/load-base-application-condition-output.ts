import { SpaceStatus } from '../../enums/bitlock-common-service/space-status';
import { SumamoruApplicationStatus } from '../../enums/bitlock-common-service/sumamoru-application-status';

export interface LoadBaseApplicationConditionOutput {
  id: string; // all_space_id
  name: string; // 物件名
  code: string; // 物件コード
  buildingName: string; // 物件名
  postCode: string; // 郵便番号
  prefecture: string; // 都道府県コード
  city: string; // 市区町村
  afterCity: string; // 市区町村以降（番地など）
  address: string; // 住所情報(prefecture, city, afterCityを繋げた表示用)
  photoUri: string; // 部屋に登録されている写真のURL
  sumamoruCode: string; // 標準採用建物登録時の建物No.
  status: SpaceStatus; // 空間ステータス定義
  hasElectricContract: boolean; // 電気契約しているかどうか
  occupyScheduledDate: number; // 入居予定日
  applicationStatus: SumamoruApplicationStatus; // 申請状況
  managementCompanyName: string; // 申込みサイトに表示する管理会社名
  managementCompanyAddress: string; // 申込みサイトに表示する管理会社住所
  managementCompanyPhoneNumber: string; // 申込みサイトに表示する管理会社電話番号
}
