import { ThunkAction } from 'redux-thunk';
import * as Actions from './actions';
import { AnyAction } from 'redux';
import { AppState } from '../../store';
import moment from 'moment';
import { bitlockApiPath, ifBaseApiPath } from '../../config/baseConfig';
import { ItemValueKey } from '../../config/item-value-key';

export const submitParamater = (): ThunkAction<Promise<void>, AppState, {}, AnyAction> => async (
  dispatch,
  getState
) => {
  try {
    dispatch(Actions.submitParamater());

    const email = getState().user.email;
    const phoneNumber = getState().user.phoneNumber;
    const access_token = getState().user.access_token;
    const refresh_token = getState().user.refresh_token;
    const user_id = getState().user.user_id;
    const propParam = getState().property.property;
    const method = 'POST';
    const bitlockManageEndpoint = `${bitlockApiPath}/users`;
    const headers = {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'x-api-key': access_token
    };
    let body = JSON.stringify({
      bkp_user_id: user_id,
      email: email,
      phone_number: phoneNumber
    });

    let status = 0;

    const bitlockRes = await fetch(bitlockManageEndpoint, {
      method,
      headers,
      body,
      mode: 'cors'
    }).then(res => {
      if (res.status === 200 || res.status === 409 || res.status === 201) {
        status = res.status;
      } else if (res.status === 401) {
        dispatch(Actions.failedToSubmitParamater('unauthorized'));
        return { statusCode: res.status };
      } else {
        dispatch(Actions.failedToSubmitParamater('internal'));
        return { statusCode: res.status };
      }
      status = res.status;
    });

    if (bitlockRes !== undefined && bitlockRes.statusCode === 401) return;
    // 409は既にアカウントがある状態なので申請に進んで良い
    if (status !== 200 && status !== 409 && status !== 201) {
      dispatch(Actions.failedToSubmitParamater('internal'));
      return;
    }
    const formParam = getState().applyForm.parameters;
    const endpoint = `${ifBaseApiPath}/application/create`;

    const { isSameEmail } = getState().applyForm.parameters;
    const { isCoodinatedGasStartDate } = getState().applyForm.parameters;

    let formEmail = formParam.email;
    let formEmailConfirm = formParam.email_confirmation;

    if (isSameEmail) {
      formEmail = email;
      formEmailConfirm = email;
    }

    const bodyParameter = {
      // 認証関連
      refresh_token: refresh_token,

      // bMからURL発行時点で確定している情報
      organization_id: getOrganizationId(propParam.allSpaceId),
      property_id: propParam.allSpaceId,
      contract_id: propParam.contractId,
      property_no: propParam.sumamoruCode,
      address1: propParam.prefecture + propParam.city + propParam.afterCity,
      address2: propParam.buildingName + propParam.propertyName,
      postal_code: propParam.postCode,
      move_plan_date: moment(propParam.occupyScheduledDate).format('YYYY-MM-DD'),
      code: propParam.propertyCode,
      prefecture: propParam.prefecture,
      city: propParam.city,
      afterCity: propParam.afterCity,
      building_name: propParam.buildingName,
      name: propParam.propertyName
    };

    Object.values(ItemValueKey).forEach(valueKey => {
      bodyParameter[valueKey] = formParam[valueKey];
    });

    body = JSON.stringify(bodyParameter);

    fetch(endpoint, {
      method,
      headers,
      body,
      mode: 'cors'
    })
      .then(res => {
        if (res.status === 200) {
          dispatch(Actions.successToSubmitParamater());
        } else {
          dispatch(Actions.failedToSubmitParamater('internal'));
        }
      })
      .catch(error => {
        console.error(error);
        dispatch(Actions.failedToSubmitParamater('internal'));
      });
  } catch (error) {
    console.error(error);
    dispatch(Actions.failedToSubmitParamater('internal'));
  }
};

const getDateString = (date?: string): string | undefined => {
  if (!date || date.length === 0) {
    return undefined;
  }
  return moment(new Date(date)).format('YYYY-MM-DD');
};

const getOrganizationId = (allSpaceId: string): string | undefined => {
  if (!allSpaceId || allSpaceId.length === 0) {
    return undefined;
  }
  const splited = allSpaceId.split('_');
  if (!splited || splited.length === 0) {
    return undefined;
  }
  return splited[0];
};
