import { AppState } from '../../store';

export const getBirthday = ({ identification }: AppState) => identification.parameters.birthday;
