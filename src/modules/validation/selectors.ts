import { AppState } from '../../store';

export const getCode = ({ validation }: AppState): string => {
  return validation.code;
};

type status = 'not_submit' | 'submit_loading' | 'submit' | 'error';

export const getStatus = ({ validation }: AppState): status => {
  return validation.status;
};
