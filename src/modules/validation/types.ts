enum ActionType {
  updateCode = 'UPDATE_CODE',
  submitCode = 'SUBMIT_CODE',
  submitSuccess = 'SUBMIT_SUCCESS',
  submitFailed = 'SUBMIT_FAILED'
}

export default ActionType;
