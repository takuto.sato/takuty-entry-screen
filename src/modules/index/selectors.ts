import { AppState } from '../../store';
import { IndexStatus } from './types';

export const getStatus = ({ index }: AppState): IndexStatus => {
  return index.status;
};
