enum ActionType {
  setQueryParams = 'SET_QUERY_PARAMS',
  invalidQueryParams = 'INVALID_QUERY_PARAMS',
  clearStatus = 'CLEAR_STATUS'
}

export type IndexStatus = 'not_set' | 'query_set' | 'invalid_query';

export default ActionType;
