import { AppState } from '../../store';
import { Property } from './types';

export const getProperty = ({ property }: AppState): Property => {
  return property.property;
};
