import React, { Component } from 'react';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import { Stepper, Button, TextField, Container, Step, Input } from '@material-ui/core';
import { Link } from 'react-router-dom';
import { Path } from '../../constants/path';

const IndexScreen: React.FC<{}> = () => {
  return (
    <div>
      <Container>
        <div
          style={{
            textAlign: 'center',
            background: '#ECEFF1',
            padding: 10,
            borderRadius: 14
          }}
        >
          <span>Bitkeyアカウントを作成</span>
          <TextField
            style={{ background: 'white', margin: 8, width: '90%' }}
            placeholder="メールアドレス"
            margin="normal"
            InputLabelProps={{
              shrink: true
            }}
            variant="outlined"
          />
          <TextField
            style={{ background: 'white', margin: 8, width: '90%' }}
            placeholder="パスワード"
            type="password"
            margin="normal"
            InputLabelProps={{
              shrink: true
            }}
            variant="outlined"
          />
          <div style={{ margin: '0 auto', width: '90%', textAlign: 'left' }}>
            利用規約とプライバシーポリシーに同意の上、登録してください。
          </div>
          <Button style={{ width: '90%', color: 'white', background: '#029FAC' }} size="large">
            Bitkeyアカウントを作成
          </Button>
        </div>
        Bitkeyアカウントをお持ちの方は
        <Link to={Path.account.completed}>ログイン</Link>
      </Container>
    </div>
  );
};

export default IndexScreen;
