import { ContainerElement, ComponentElementCondition } from '../types/common/item-definition-types';
import { ItemValueKey } from './item-value-key';
import * as moment from 'moment-timezone';
import { ConfirmationDisplayType } from '../enums/confirmation/confirmation-display-type';
import { ComponentDescriptionType } from '../enums/common/component-description-type';
import { ValueOriginType } from '../enums/common/value-origin-type';
import { ComponentType } from '../enums/common/component-type';
import { DetailConditionOperator } from '../enums/common/detail-condition-operator';

export const dateDisplayFormat = 'YYYY年MM月DD日(ddd)';

const useGasCondition: Array<ComponentElementCondition> = [
  {
    detailConditions: [
      {
        valueType: ValueOriginType.InputValues,
        valueKey: ItemValueKey.UseGasPlanSelection,
        operator: DetailConditionOperator.NotEqual,
        targetValues: [
          {
            valueType: ValueOriginType.Fixed,
            valueKey: '1'
          }
        ]
      }
    ]
  }
];
const notUseGasCondition: Array<ComponentElementCondition> = [
  {
    detailConditions: [
      {
        valueType: ValueOriginType.InputValues,
        valueKey: ItemValueKey.UseGasPlanSelection,
        operator: DetailConditionOperator.Equal,
        targetValues: [
          {
            valueType: ValueOriginType.Fixed,
            valueKey: '1'
          }
        ]
      }
    ]
  }
];

const notUseBasicGasPlan: Array<ComponentElementCondition> = [
  {
    detailConditions: [
      {
        valueType: ValueOriginType.InputValues,
        valueKey: ItemValueKey.GasBasePlanSelection,
        operator: DetailConditionOperator.Equal,
        targetValues: [
          {
            valueType: ValueOriginType.Fixed,
            valueKey: '1'
          }
        ]
      }
    ]
  }
];

// 申込画面に表示するコンポーネント定義
export const RegisteredInputContainers: Array<ContainerElement> = [
  {
    id: 'SelectBasicPlan',
    title: 'CDエナジーの都市ガスもセットで申し込みますか？',
    confirmationTitle: '都市ガスの申し込み',
    confirmationDisplayType: ConfirmationDisplayType.EmphasizedDisableInput,
    components: [
      {
        id: ItemValueKey.UseGasPlanSelection,
        label: undefined,
        descriptionsBeforeInput: [
          {
            descriptionType: ComponentDescriptionType.Balloon,
            messages: ['ガスと電気のセット契約で、', '電気料金がオトクになります ']
          }
        ],
        inputElement: {
          valueKey: ItemValueKey.UseGasPlanSelection,
          inputType: ComponentType.RadioButtonInput,
          initialValue: '0',
          selectionItems: [
            {
              value: '0',
              label: 'ガスの申込も一緒に行う'
            },
            {
              value: '1',
              label: 'ガスの申込はしない/プロパンガス物件'
            }
          ]
        },
        descriptionsAfterInput: [
          {
            messages: [
              '※プロパンガスの物件の場合は、ガスの開栓は申込みしないを登録してください。',
              '※都市ガスの受付は、東京ガスの供給エリア (日立市を除く東京地区) に限ります。'
            ]
          }
        ]
      }
    ]
  },
  {
    id: 'MoveInfo',
    title: 'お引越し情報',
    components: [
      {
        id: ItemValueKey.OccupyScheduledDate,
        label: '入居予定日',
        inputElement: {
          valueKey: ItemValueKey.OccupyScheduledDate,
          valueType: ValueOriginType.ApplicationParams,
          inputType: ComponentType.FixedText,
          converter: value =>
            moment(value)
              .tz('Asia/Tokyo')
              .format(dateDisplayFormat)
        }
      },
      {
        id: ItemValueKey.ElectricityStartDate,
        label: '電気：利用開始日',
        inputElement: {
          valueKey: ItemValueKey.ElectricityStartDate,
          inputType: ComponentType.KeyboardDateInput,
          minDateOffset: 2
        },
        descriptionsAfterInput: [
          {
            messages: [
              '※開栓希望日が第２営業日（土日祝除く平日）以内に電気のご利用をご希望の場合、ＣＤエナジーダイレクトのお客さまセンターへお電話いただき、「スマモル賃貸プラン」を希望とお伝えください。',
              '連絡先：０１２０－８１１－７９２'
            ]
          }
        ]
      },
      {
        id: ItemValueKey.ElectricityBasePlanSelection,
        label: '電気お申込みプラン',
        inputElement: {
          valueKey: ItemValueKey.ElectricityBasePlanSelection,
          inputType: ComponentType.RadioButtonInput,
          initialValue: '0',
          selectionItems: [
            {
              value: '0',
              label: 'スマモル賃貸プランB'
            },
            {
              value: '1',
              label: 'スマモル賃貸プランAE'
            }
          ]
        },
        descriptionsAfterInput: [
          {
            messages: [
              '※スマモル賃貸プランBは東京電力の従量電灯B相当、スマモル賃貸プランAEは、オール電化のお客さま向け料金メニューです。',
              '※ご契約の容量は、お住いになるマンションの契約電流となります。'
            ]
          }
        ]
      },
      {
        id: ItemValueKey.ElectricityOptionPlanSelection,
        label: '電気オプション割引',
        inputElement: {
          valueKey: ItemValueKey.UseGasPlanSelection,
          inputType: ComponentType.RadioButtonInput,
          initialValue: '0',
          selectionItems: [
            {
              value: '0',
              label: 'ガスセット割引'
            },
            {
              value: '1',
              label: 'なし',
              notDisplay: true
            }
          ],
          /* ガス利用の場合のみに表示される条件 */
          isDisableCondition: notUseGasCondition
        },
        descriptionsAfterInput: [
          {
            messages: ['※ガスとセットで申し込まれる場合、「ガスセット割」が適用されます。']
          }
        ]
        /* ガス利用の場合のみに表示される条件 */
        // isDisplayCondition: useGasCondition
      },
      {
        id: ItemValueKey.GasStartDate,
        label: 'ガス：開栓希望日・時間帯',
        descriptionsBeforeInput: [
          {
            messages: ['※ガス開栓にはお客様の立ち会いが必要となります。']
          }
        ],
        inputElement: {
          valueKey: ItemValueKey.GasStartDate,
          inputType: ComponentType.KeyboardDateInput,
          minDateOffset: 5
        },
        showOnlyInput: true,
        /* ガス利用の場合のみに表示される条件 */
        isDisplayCondition: useGasCondition
      },
      {
        id: ItemValueKey.GasStartDateTime,
        label: 'ガス:¥n開栓希望日・時間帯',
        inputElements: [
          {
            valueKey: ItemValueKey.GasStartDate,
            inputType: ComponentType.KeyboardDateInput,
            minDateOffset: 5
          },
          {
            valueKey: '¥n',
            inputType: ComponentType.FixedText,
            valueType: ValueOriginType.Fixed
          },
          {
            valueKey: ItemValueKey.GasStartTimeSelection,
            inputType: ComponentType.SelectItemInput,
            selectionItems: [
              {
                value: '0',
                label: 'ＡＭ'
              },
              {
                value: '1',
                label: 'ＰＭ（１３-１５時）'
              },
              {
                value: '2',
                label: 'ＰＭ（１５-１７時）'
              },
              {
                value: '3',
                label: 'ＥＶＥ（１７-１９時）'
              },
              {
                value: '4',
                label: 'ＰＭ（１３-１７時）'
              }
            ]
          }
        ],
        showOnlyConfirmation: true
      },
      {
        id: ItemValueKey.GasStartTimeSelection,
        inputElement: {
          valueKey: ItemValueKey.GasStartTimeSelection,
          inputType: ComponentType.SelectItemInput,
          initialValue: '0',
          selectionItems: [
            {
              value: '0',
              label: 'ＡＭ'
            },
            {
              value: '1',
              label: 'ＰＭ（１３-１５時）'
            },
            {
              value: '2',
              label: 'ＰＭ（１５-１７時）'
            },
            {
              value: '3',
              label: 'ＥＶＥ（１７-１９時）'
            },
            {
              value: '4',
              label: 'ＰＭ（１３-１７時）'
            }
          ],
          styleOption: {
            marginTop: 0
          }
        },
        descriptionsAfterInput: [
          {
            messages: [
              '※開栓希望日が第５営業日（土日祝除く平日）以内にガスの開栓をご希望の場合、ＣＤエナジーダイレクトで受付が出来ません。申し訳ございませんが、東京ガスへお申込みをお願いします。'
            ]
          }
        ],
        showOnlyInput: true,
        /* ガス利用の場合のみに表示される条件 */
        isDisplayCondition: useGasCondition
      },
      {
        id: ItemValueKey.WitnessGasStart,
        label: '開栓の立ち会い者',
        inputElement: {
          valueKey: ItemValueKey.WitnessGasStart,
          inputType: ComponentType.SelectItemInput,
          initialValue: '0',
          selectionItems: [
            {
              value: '0',
              label: '本人'
            },
            {
              value: '1',
              label: '家主'
            },
            {
              value: '2',
              label: '管理人'
            },
            {
              value: '3',
              label: 'その他'
            }
          ]
        },
        /* ガス利用の場合のみに表示される条件 */
        isDisplayCondition: useGasCondition
      },
      {
        id: ItemValueKey.WitnessNameGasStart,
        label: '立ち会い者氏名',
        inputElement: {
          valueKey: ItemValueKey.WitnessNameGasStart,
          inputType: ComponentType.TextFieldInput,
          placeholder: '性 名'
        },
        isDisplayCondition: [
          {
            detailConditions: [
              {
                valueType: ValueOriginType.InputValues,
                valueKey: ItemValueKey.WitnessGasStart,
                operator: DetailConditionOperator.Some,
                targetValues: [
                  {
                    valueType: ValueOriginType.Fixed,
                    valueKey: '1'
                  },
                  {
                    valueType: ValueOriginType.Fixed,
                    valueKey: '2'
                  },
                  {
                    valueType: ValueOriginType.Fixed,
                    valueKey: '3'
                  }
                ]
              }
            ]
          },
          ...useGasCondition
        ]
      },
      {
        id: ItemValueKey.TelForGasStart,
        label: 'ガス開栓日に連絡可能な電話番号',
        inputElement: {
          valueKey: ItemValueKey.TelForGasStart,
          inputType: ComponentType.TextFieldInput,
          placeholder: '08012345678',
          inputComponentType: 'tel'
        },
        descriptionsAfterInput: [
          {
            messages: ['※立ち会い者と連絡可能な電話番号を入力ください']
          }
        ],
        /* ガス利用の場合のみに表示される条件 */
        isDisplayCondition: useGasCondition
      },
      {
        id: ItemValueKey.GasBasePlanSelection,
        label: 'ガスお申込みプラン',
        inputElement: {
          valueKey: ItemValueKey.GasBasePlanSelection,
          inputType: ComponentType.RadioButtonInput,
          initialValue: '0',
          selectionItems: [
            {
              value: '0',
              label: 'ベーシックガス'
            },
            {
              value: '1',
              label: 'ゆかぽかガス'
            }
          ]
        },
        descriptionsAfterInput: [
          {
            messages: [
              '※ベーシックガスは東京ガスの一般料金、ゆかぽかガスはガス温水式床暖房をご利用のお客さま向け料金です。'
            ]
          }
        ],
        /* ガス利用の場合のみに表示される条件 */
        isDisplayCondition: useGasCondition
      },
      {
        id: ItemValueKey.GasOptionPlanSelection,
        label: 'ベーシックガスのオプション割引',
        inputElement: {
          valueKey: ItemValueKey.GasBasePlanSelection,
          inputType: ComponentType.RadioButtonInput,
          initialValue: '0',
          selectionItems: [
            {
              value: '0',
              label: '電気セット割引'
            },
            {
              value: '1',
              label: 'なし',
              notDisplay: true
            }
          ],
          /* ガス利用の場合のみに表示される条件 */
          isDisableCondition: notUseBasicGasPlan
        },
        descriptionsAfterInput: [
          {
            messages: ['※スマモル賃貸プランとベーシックガスを申し込みの場合、「電気セット割引」が適用されます。']
          }
        ],
        /* ガス利用の場合のみに表示される条件 */
        isDisplayCondition: [
          {
            detailConditions: [
              {
                valueType: ValueOriginType.InputValues,
                valueKey: ItemValueKey.GasBasePlanSelection,
                operator: DetailConditionOperator.NotEqual,
                targetValues: [
                  {
                    valueType: ValueOriginType.Fixed,
                    valueKey: '1'
                  }
                ]
              }
            ]
          },
          ...useGasCondition
        ]
      },
      {
        id: 'gas_option_discount2',
        label: 'ゆかぽかガスのオプション割引',
        descriptionsAfterInput: [
          {
            messages: [
              '※ゆかぽかガスのオプション割引の適用を希望される場合は、本お申込み後、ＣＤエナジーダイレクトのお客さまセンターへご連絡をお願いします。',
              '連絡先：０１２０－８１１－７９２'
            ]
          }
        ],
        showOnlyInput: true,
        /* ガス利用の場合のみに表示される条件 */
        isDisplayCondition: [
          {
            detailConditions: [
              {
                valueType: ValueOriginType.InputValues,
                valueKey: ItemValueKey.GasBasePlanSelection,
                operator: DetailConditionOperator.Equal,
                targetValues: [
                  {
                    valueType: ValueOriginType.Fixed,
                    valueKey: '1'
                  }
                ]
              }
            ]
          },
          ...useGasCondition
        ]
      }
      // {
      //   id: 'gas_option_discount2',
      //   label: 'ガスオプション割引2',
      //   inputElement: {
      //     valueKey: 'gas_option_discount2',
      //     inputType: ComponentInputType.RadioButton,
      //     initialValue: '3',
      //     selectionItems: [
      //       {
      //         value: '0',
      //         label: '浴室暖房割'
      //       },
      //       {
      //         value: '1',
      //         label: 'エコ給湯割'
      //       },
      //       {
      //         value: '2',
      //         label: 'ダブル割'
      //       },
      //       {
      //         value: '3',
      //         label: '割引なし'
      //       }
      //     ]
      //   },
      //   descriptionsAfterInput: [
      //     {
      //       descriptionType: ComponentDescriptionType.Normal,
      //       messages: [
      //         '※浴室暖房割はガス浴室暖房乾燥機をご利用のお客様向け、エコ給湯割は家庭用省エネ給湯器をご利用のお客様向け、ダブル割はガス浴室暖房乾燥機と家庭用省エネ給湯器の両方をご利用のお客様向けのオプション割引です。'
      //       ]
      //     }
      //   ],
      //   /* ガス利用の場合のみに表示される条件 */
      //   isDisplayCondition: [
      //     {
      //       detailConditions: [
      //         {
      //           valueType: ComponentInputValueType.InputValues,
      //           valueKey: 'gassPlan',
      //           operator: ComponentConditionOperator.Equal,
      //           targetValues: [
      //             {
      //               valueType: ComponentInputValueType.Fixed,
      //               valueKey: '1'
      //             }
      //           ]
      //         }
      //       ]
      //     },
      //     ...useGasCondition
      //   ]
      // }
    ]
  },
  {
    id: 'CustomerInformation',
    title: 'お客様情報',
    components: [
      {
        id: ItemValueKey.CustomerNamePrefix,
        inputElement: {
          valueKey: ItemValueKey.CustomerNamePrefix,
          inputType: ComponentType.KanaNameAutoCompleteInput
        },
        showOnlyInput: true
      },
      {
        id: ItemValueKey.CustomerName,
        label: '氏名',
        inputElements: [
          {
            valueKey: ItemValueKey.CustomerLastName,
            inputType: ComponentType.TextFieldInput
          },
          {
            valueKey: ItemValueKey.CustomerFirstName,
            inputType: ComponentType.TextFieldInput
          }
        ],
        showOnlyConfirmation: true
      },
      {
        id: ItemValueKey.CustomerNameKana,
        label: '氏名(カナ)',
        inputElements: [
          {
            valueKey: ItemValueKey.CustomerLastNameKana,
            inputType: ComponentType.TextFieldInput
          },
          {
            valueKey: ItemValueKey.CustomerFirstNameKana,
            inputType: ComponentType.TextFieldInput
          }
        ],
        showOnlyConfirmation: true
      },
      {
        id: ItemValueKey.CustomerBirthDay,
        label: '生年月日',
        inputElement: {
          valueKey: ItemValueKey.CustomerBirthDay,
          inputType: ComponentType.SelectDateInput
        }
      },
      {
        id: ItemValueKey.CustomerTel,
        label: '電話番号',
        inputElement: {
          valueKey: ItemValueKey.CustomerTel,
          inputType: ComponentType.TextFieldInput,
          placeholder: '08012345678',
          inputComponentType: 'tel'
        }
      },
      {
        id: ItemValueKey.CustomerGender,
        label: '性別',
        inputElement: {
          valueKey: ItemValueKey.CustomerGender,
          inputType: ComponentType.SelectItemInput,
          placeholder: '性別を選択',
          initialValue: '',
          selectionItems: [
            {
              value: '0',
              label: '男性'
            },
            {
              value: '1',
              label: '女性'
            },
            {
              value: '2',
              label: '無回答'
            }
          ]
        }
      },
      {
        id: ItemValueKey.CustomerEmail,
        label: 'メールアドレス',
        inputElement: {
          valueKey: ItemValueKey.CustomerEmail,
          inputType: ComponentType.EmailInput
        }
      }
    ]
  },
  {
    id: ItemValueKey.PaymentMethodSelection,
    title: 'お支払い方法',
    confirmationTitle: 'お支払い手続き',
    confirmationDisplayType: ConfirmationDisplayType.EmphasizedDisableInput,
    components: [
      {
        id: 'paymentInfo',
        label: '口座振替・クレジットカードをご選択の場合、CDエナジーより申込書を郵送します。',
        confirmationLabel: '',
        descriptionsBeforeInput: [
          {
            messages: ['※お手続きが完了するまでは払込票でのお支払いとなります。']
          }
        ],
        inputElement: {
          valueKey: ItemValueKey.PaymentMethodSelection,
          inputType: ComponentType.RadioButtonInput,
          initialValue: '0',
          selectionItems: [
            {
              value: '0',
              label: '口座振替'
            },
            {
              value: '1',
              label: 'クレジットカード'
            }
          ]
        }
      }
    ]
  }
];

export const ableGoToNextPageConditionDefinition = {
  needToInput: {
    inputValues: [
      ItemValueKey.ElectricityStartDate,
      ItemValueKey.GasStartDate,
      ItemValueKey.WitnessNameGasStart,
      ItemValueKey.TelForGasStart,
      ItemValueKey.CustomerFirstName,
      ItemValueKey.CustomerLastName,
      ItemValueKey.CustomerFirstNameKana,
      ItemValueKey.CustomerLastNameKana,
      ItemValueKey.CustomerTel,
      ItemValueKey.CustomerGender,
      ItemValueKey.CustomerEmailInput,
      ItemValueKey.CustomerEmailConfirmation
    ]
  },
  hasNoError: {
    inputValues: [
      ItemValueKey.ElectricityStartDate,
      ItemValueKey.GasStartDate,
      ItemValueKey.TelForGasStart,
      ItemValueKey.CustomerFirstName,
      ItemValueKey.CustomerLastName,
      ItemValueKey.CustomerFirstNameKana,
      ItemValueKey.CustomerLastNameKana,
      ItemValueKey.CustomerTel,
      ItemValueKey.CustomerEmailInput,
      ItemValueKey.CustomerEmailConfirmation
    ]
  },
  satisfySpecificCondition: [
    {
      detailConditions: [
        {
          valueType: ValueOriginType.InputValues,
          valueKey: ItemValueKey.AgreeThePolicy,
          operator: DetailConditionOperator.Equal,
          targetValues: [
            {
              valueType: ValueOriginType.Fixed,
              valueKey: true
            }
          ]
        }
      ]
    }
  ]
};
