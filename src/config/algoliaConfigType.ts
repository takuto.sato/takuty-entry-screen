export type AlgoliaConfigType = {
  AllSpace: AlgoliaConfigTableUnit;
};

export type AlgoliaConfigTableUnit = {
  applicationId: string;
  searchOnlyApiKey: string;
  adminApiKey: string;
  monitoringApiKey: string;
};
