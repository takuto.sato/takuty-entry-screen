export const phoneRegex = /^0{1}\d{8,13}$/;
export const onlyNumberRegex = /^\d+$/;
export const zenkakuRegex = /^[^\x01-\x7E\uFF65-\uFF9F]+$/;
export const zenkakuKanaRegex = /^[\u30a1-\u30f6]+$/;
export const mailRegex = /^[A-Za-z0-9]{1}[A-Za-z0-9_.-]*@{1}[A-Za-z0-9_.-]{1,}\.[A-Za-z0-9]{1,}$/;
