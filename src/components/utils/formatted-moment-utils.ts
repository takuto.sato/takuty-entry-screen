import MomentUtils from '@date-io/moment';
import { Moment } from 'moment';

export default class FormattedMomentUtils extends MomentUtils {
  public getCalendarHeaderText = (date: Moment): string => {
    return date.format('YYYY年 M月');
  };
}
