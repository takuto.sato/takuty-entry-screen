import React from 'react';

type PropsFromParent = {
  title: string;
  detail?: string;
};
const BaseInputExp: React.FC<PropsFromParent> = props => {
  return (
    <div>
      <h3>{props.title}</h3>
      {props.detail ? <p>{props.detail}</p> : ''}
    </div>
  );
};

export default BaseInputExp;
