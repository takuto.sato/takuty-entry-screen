import * as React from 'react';
import { useMemo, useContext } from 'react';
import { getConfirmationInputText } from '../utils/key-value-getter';
import { BaseFormStateContext } from '../BaseForm';
import { ComponentElement } from '../../types/common/item-definition-types';
import OccupancyLabel from '../OccupancyLabel';
import { ConfimationFormStateContext } from '../template/ConfirmTemplate';
import { Table, Tr } from '../../components/Table';

interface P {
  component: ComponentElement;
}

const ConfirmationListView: React.FC<P> = ({ component }) => {
  const { inputValues, applicationParams } = useContext(ConfimationFormStateContext);

  if (component.descriptionsBeforeInput) {
    return <></>;
  }

  const valueKey = useMemo(() => (component.inputElement && component.inputElement.valueKey) || '', []);

  const displayText = useMemo(() => {
    if (!component.inputElement) {
      return '';
    }
    return getConfirmationInputText({
      inputValues,
      applicationParams,
      inputValueType: component.inputElement.valueType,
      valueKey: valueKey,
      initialValue: component.inputElement.initialValue,
      converter: component.inputElement.converter,
      inputComponentType: component.inputElement.inputType,
      selectionItems: component.inputElement.selectionItems
    });
  }, [inputValues[valueKey], applicationParams[valueKey], component, valueKey]);

  return <Tr th={component.label || ''} td={displayText} />;
};

export default React.memo(ConfirmationListView);
