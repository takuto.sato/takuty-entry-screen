import React, { FC, useState, useEffect, useCallback, useMemo, createContext } from 'react';
import { Container, FormControlLabel, Checkbox, Button } from '@material-ui/core';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import OccupancyLabel, { OccupancyLabelBox } from './OccupancyLabel';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';
import DateFnsUtils from '@date-io/date-fns';
import 'date-fns';
import { ApplyHeading, ApplyLabel, ApplyCaption } from '../components/Label';
import { CustomRadioButton, BlueRadio } from './CustomRadioButton';
import { CustomKeyButton } from './CustomButton';
import { mapStateToProps, mapDispatchToProps } from '../container/applyForm';
import * as moment from 'moment-timezone';
import { CustomTextField } from './inputs/CustomTextField';
import ApplicationModal from './ApplyModal';
import { ApplyTextField } from './baseform/ApplyTextField';
import BackModal from '../container/backModal';
import { MuiPickersOverrides } from '@material-ui/pickers/typings/overrides';
import jaLocale from 'date-fns/locale/ja';
import * as AutoKana from 'vanilla-autokana';
import { Link } from 'react-router-dom';
import { Path } from '../constants/path';
import ComponentElement from './componentElements/ComponentElement';
import { RegisteredInputContainers, ableGoToNextPageConditionDefinition } from '../config/iten-dedinitions';

import { ItemValueKeySuffix } from '../enums/common/item-value-key-suffix';
import { ItemValueKey } from '../config/item-value-key';
import { checkComponentElementCondition } from './utils/check-component-element-condition';

type overridesNameToClassKey = {
  [P in keyof MuiPickersOverrides]: keyof MuiPickersOverrides[P];
};

declare module '@material-ui/core/styles/overrides' {
  export interface ComponentNameToClassKey extends overridesNameToClassKey {}
}

const useStyles = makeStyles(
  createStyles({
    container: {
      textAlign: 'left',
      padding: '0 0 20px',
      '& .MuiOutlinedInput-notchedOutline': {
        borderColor: '#E0E0E0'
      },
      '& .Mui-focused': {
        '& .MuiOutlinedInput-notchedOutline': {
          borderColor: 'var(--color-key)'
        }
      }
    },
    confirmButton: {
      margin: '24px 0 0'
    },
    checkbox: {
      '& .MuiTypography-root': {
        color: 'var(--color-text)',
        fontSize: 14
      },
      '& .MuiCheckbox-root': {
        color: 'var(--color-key)'
      }
    },
    agreeCheckbox: {
      display: 'block',
      margin: 0,
      textAlign: 'center',
      '& .MuiTypography-root': {
        color: 'var(--color-text)',
        fontSize: 14
      },
      '& .MuiCheckbox-root': {
        color: 'var(--color-key)'
      }
    },
    backButtonContainer: {
      margin: '40px 0',
      textAlign: 'center'
    },
    backButton: {
      color: 'var(--color-gray-3)',
      textDecoration: 'none'
    },
    termsCaption: {
      fontSize: 14,
      lineHeight: '21px',
      color: 'var(--color-text)',
      '& a': {
        textDecoration: 'none',
        color: 'var(--color-key)',
        fontWeight: 'bold'
      }
    }
  })
);

export const BaseFormStateContext = createContext<{
  inputValues: any;
  updateInputValues: any;
  applicationParams: any;
}>({
  inputValues: {},
  // tslint:disable-next-line: no-empty
  updateInputValues: () => {},
  applicationParams: {}
});

type Props = ReturnType<typeof mapDispatchToProps> & ReturnType<typeof mapStateToProps>;

const BaseForm: FC<Props> = ({
  inputValues,
  updateInputValues,
  applicationParams,

  use_og_gas,
  management_company_address,
  management_company_name,
  management_company_phonenumber,
  birthday,
  email,
  email_confirmation,
  pay_method,
  move_plan_date,
  updateParameter,
  property,
  gender,
  bitkeyEmail,
  bitkeyPhoneNumber,
  initialBirthday,
  isSameEmail,
  getTokens,
  getIds,
  getProperty
}) => {
  const [modalOpen, setModalOpen] = useState(false);
  const [backModalOpen, setBackModalOpen] = useState(false);

  const styles = useStyles({});

  // TODO 最終的に組み込みを行う
  // /**
  //  * 電話番号でサインイン、サインアップした場合に実行する初期化処理
  //  */
  // useEffect(() => {
  //   if (bitkeyPhoneNumber) {
  //     updateParameter('isSameEmail', false);
  //     updateParameter('phone_number', bitkeyPhoneNumber);
  //     updateParameter('phone_number_when_move', bitkeyPhoneNumber);
  //   }
  // }, [updateParameter, bitkeyPhoneNumber]);

  // /**
  //  * 本人確認に利用した誕生日を初期値に詰めておく
  //  */
  // useEffect(() => {
  //   if (initialBirthday) {
  //     updateParameter('birthday', initialBirthday);
  //     const initialYear = initialBirthday.slice(0, 4);
  //     const initialMonth = initialBirthday.slice(4, 6);
  //     const initialDay = initialBirthday.slice(6, 8);
  //     setYear(Number(initialYear));
  //     setMonth(Number(initialMonth));
  //     setDay(Number(initialDay));
  //   }
  // }, [updateParameter, initialBirthday]);

  useEffect(() => {
    if (property.allSpaceId === '') {
      getIds();
    }
    if (bitkeyEmail === '') {
      getTokens();
    }
    if (bitkeyEmail !== '' && property.allSpaceId !== '' && property.address === '') {
      getProperty();
    }
  }, [bitkeyEmail, property, getIds, getTokens, getProperty]);

  const baseFormStateContext = useMemo(
    () => ({
      inputValues: inputValues,
      updateInputValues: updateInputValues,
      applicationParams: applicationParams
    }),
    [inputValues, updateInputValues, applicationParams]
  );

  const ableToGoToNextPage = useMemo(() => {
    // 入力必須の項目が入力されているか否か、
    const allInput = ableGoToNextPageConditionDefinition.needToInput.inputValues.every(key => {
      // 項目が表示されていない、または、入力向こうの場合には値とチェックする必要がないのでtrueを返却
      if (
        inputValues[key + ItemValueKeySuffix.IsDisplayed] === false ||
        inputValues[key + ItemValueKeySuffix.IsDisabled]
      ) {
        return true;
      }
      return !!inputValues[key] && inputValues[key] !== '';
    });

    // Errorとなっている項目があるか否か
    const hasNoError = ableGoToNextPageConditionDefinition.hasNoError.inputValues.every(key => {
      // 項目が表示されていない、または、入力向こうの場合には値とチェックする必要がないのでtrueを返却
      if (
        inputValues[key + ItemValueKeySuffix.IsDisplayed] === false ||
        inputValues[key + ItemValueKeySuffix.IsDisabled]
      ) {
        return true;
      }
      return !inputValues[key + ItemValueKeySuffix.HasError];
    });

    const satisfySpecificCondition = checkComponentElementCondition({
      conditions: ableGoToNextPageConditionDefinition.satisfySpecificCondition,
      inputValues: inputValues,
      applicationParams: applicationParams,
      initialValue: false
    });
    // satisfySpecificCondition

    console.log('allInput', allInput);
    console.log('hasNoError', hasNoError);

    return allInput && hasNoError && satisfySpecificCondition;
  }, [inputValues]);

  return (
    <div>
      <BaseFormStateContext.Provider value={baseFormStateContext}>
        {RegisteredInputContainers.map(containerElement => (
          <Container className={styles.container} key={containerElement.id}>
            {containerElement.title && <ApplyHeading id={containerElement.id} text={containerElement.title} />}
            {containerElement.components &&
              containerElement.components
                .filter(component => !component.showOnlyConfirmation)
                .map(component => <ComponentElement component={component} key={component.id} />)}
          </Container>
        ))}
      </BaseFormStateContext.Provider>

      <p className={styles.termsCaption}>
        上記でご入力いただいた情報は、
        <br />
        ・優待・割引サービス
        <br />
        ・駆けつけサービス
        <br />
        で利用されます。また、お客様が入居契約した物件の管理会社もご入力いただいた情報を利用します。
        <br />
        <br />
        <Link rel="noopener noreferrer" target="_blank" to={Path.other.terms}>
          優待サービス利用規約
        </Link>
        ・
        <Link rel="noopener noreferrer" target="_blank" to={Path.other.terms}>
          サービス契約約款
        </Link>
        ・
        <a target="_blank" rel="noopener noreferrer" href={'https://www.osakagas.co.jp/info/privacy.html'}>
          プライバシーポリシー
        </a>
        にご同意の上、申込内容のご確認へお進みください。
        <br />
        <FormControlLabel
          className={styles.agreeCheckbox}
          control={
            <Checkbox
              color="primary"
              checked={inputValues[ItemValueKey.AgreeThePolicy] || false}
              onChange={e => updateInputValues(ItemValueKey.AgreeThePolicy, e.target.checked)}
            />
          }
          label={'同意する'}
        />
      </p>

      <CustomKeyButton
        disabled={!ableToGoToNextPage}
        className={styles.confirmButton}
        variant="extended"
        size="large"
        onClick={() => setModalOpen(true)}
      >
        申込内容の確認へ
      </CustomKeyButton>

      <div className={styles.backButtonContainer}>
        <Button className={styles.backButton} onClick={() => setBackModalOpen(true)}>
          戻る
        </Button>
      </div>

      <BackModal isOpen={backModalOpen} setIsOpen={e => setBackModalOpen(e)} />
      <ApplicationModal
        modalOpen={modalOpen}
        setModalOpen={setModalOpen}
        management_company_address={management_company_address}
        management_company_name={management_company_name}
        management_company_phonenumber={management_company_phonenumber}
      />
    </div>
  );
};

export default BaseForm;
