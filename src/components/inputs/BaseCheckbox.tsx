import React from 'react';
import { Checkbox, FormControlLabel } from '@material-ui/core';
import { makeStyles, createStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(
  createStyles({
    checkbox: {
      '& .MuiTypography-root': {
        color: 'var(--color-text)',
        fontSize: 14
      },
      '& .MuiCheckbox-root': {
        color: 'var(--color-key)'
      }
    }
  })
);

interface Props {
  input: boolean;
}

export const BaseCheckbox = ({ input, onChange, label, meta: { touched, error } }) => {
  const styles = useStyles({});

  return (
    <FormControlLabel
      className={styles.checkbox}
      control={
        <Checkbox color="primary" checked={input.value ? true : false} onChange={e => onChange(e.target.checked)} />
      }
      label={label}
    />
  );
};
