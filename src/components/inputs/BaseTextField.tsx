import { TextField } from '@material-ui/core';
import React from 'react';

export const BaseTextField = ({ input, label, meta: { touched, error }, ...custom }) => (
  <TextField label={label} margin="normal" variant="outlined" {...input} {...custom} style={{ width: '100%' }} />
);
