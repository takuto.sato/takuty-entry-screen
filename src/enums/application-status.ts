export enum ApplicationStatus {
  Initial = 'initial',
  Submit = 'submit',
  NotSummit = 'not_submit',
  SubmitLoading = 'submit_loading',
  Conflict = 'conflict',
  Unauthorized = 'unauthorized',
  Internal = 'internal'
}
