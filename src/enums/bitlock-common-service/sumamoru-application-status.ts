export declare enum SumamoruApplicationStatus {
  application_request = 'application_request',
  application_request_canceled = 'application_request_canceled',
  applied = 'applied',
  applied_canceled = 'applied_canceled'
}
