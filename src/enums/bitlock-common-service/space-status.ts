export declare enum SpaceStatus {
  vacant = 'vacant',
  applied = 'applied',
  occupyScheduled = 'occupyScheduled',
  occupied = 'occupied',
  leaveScheduled = 'leaveScheduled',
  leaveProcessing = 'leaveProcessing',
  closed = 'closed',
  none = 'none'
}
