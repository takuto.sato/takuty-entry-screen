// 指定のValue値がどの情報によるものかを指定するためのもの
export enum ValueOriginType {
  Fixed = 'Fixed', // 固定値
  InputValues = 'InputValues', // 入力欄で入力した値
  ApplicationParams = 'ApplicationParams' // 申請前から予め確定されたパラメータ
}
