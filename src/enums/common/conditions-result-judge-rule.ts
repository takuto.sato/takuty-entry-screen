// 複数の条件の結果をもとに、特定の結果を返すさいのルール
// デフォルトは原則すべての条件が達成されたときのみTrueとなるEvery
export enum ConditionsResultJudgeRule {
  Every = 'Every',
  Some = 'Some',
  NotEvery = 'NotEvery',
  NotSome = 'NotSome'
}
