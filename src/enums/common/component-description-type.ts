// 各項目の説明文 (helperText) の表現種別、デフォルトはノーマル
export enum ComponentDescriptionType {
  Normal = 'Normal',
  Balloon = 'Balloon'
}
