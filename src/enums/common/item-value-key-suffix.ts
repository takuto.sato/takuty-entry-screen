// 指定のValueKeyの項目についての補足情報を、保存する際にkeyとして使用するSuffix
// ex) valueKeyが「email」の場合、エラーの有無は「emailHasError」、画面上に表示されるか否かは「emailIsDisplay」、利用可否は「emailIsDisabled」のkeyで保存される
export enum ItemValueKeySuffix {
  HasError = 'HasError',
  IsDisplayed = 'IsDisplayed',
  IsDisabled = 'IsDisabled'
}
