// 詳細条件の特定の値と比較対象の値との比較方法
export enum DetailConditionOperator {
  Equal = 'Equal',
  NotEqual = 'NotEqual',
  Some = 'Some'
}
