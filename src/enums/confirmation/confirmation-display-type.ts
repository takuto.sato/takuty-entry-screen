export enum ConfirmationDisplayType {
  NormalList = 'NormalList',
  EmphasizedDisableInput = 'EmphasizedDisableInput'
}
