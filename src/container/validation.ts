import { connect } from 'react-redux';
import * as Actions from '../modules/validation/actions';
import { ThunkDispatch } from 'redux-thunk';
import { AppState } from '../store';
import { AnyAction } from 'redux';
import ValidationTemplate from '../components/template/ValidationTemplate';
import * as module from '../modules/validation';
import * as signUpModule from '../modules/signUp';

export const mapStateToProps = (state: AppState) => {
  return {
    code: module.Selectors.getCode(state),
    status: module.Selectors.getStatus(state),
    signupMethod: signUpModule.Selectors.getSignupMethod(state)
  };
};

export const mapDispatchToProps = (dispatch: ThunkDispatch<AppState, {}, AnyAction>) => ({
  updateCode: (value: string) => dispatch(Actions.updateCode(value)),
  submitCode: () => dispatch(module.Operations.submitCode())
});

export default connect(mapStateToProps, mapDispatchToProps)(ValidationTemplate);
