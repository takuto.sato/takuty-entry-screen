export const Path = {
  home: '/',
  redirect: '/redirect/',
  identification: '/identification',
  account: {
    signup: '/signup',
    signin: '/signin',
    validation: '/validation',
    completed: '/account/completed'
  },
  application: {
    url: '/apply',
    confirm: '/apply/confirm',
    complete: '/apply/complete'
  },
  other: {
    terms: '/terms',
    policy: '/policy',
    sitePolicy: '/site-policy',
    tokushoho: '/commercial-transactions',
    explanation_matters: '/explanation-matters'
  }
};
